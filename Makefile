VERSION=`git describe | sed 's/^v//; s/-/./g'`
CHANGELOG_VERSION = 0.1
NAME = publik-infra

all:

DIST_FILES = \
	prometheus-entrouvert-exporter

clean:
	rm -rf sdist

dist: clean
	-mkdir sdist
	rm -rf sdist/$(NAME)-$(VERSION)
	mkdir -p sdist/$(NAME)-$(VERSION)
	for i in $(DIST_FILES); do \
		cp -R "$$i" sdist/$(NAME)-$(VERSION); \
	done

dist-bzip2: dist
	-mkdir sdist
	cd sdist && tar cfj ../sdist/$(NAME)-$(VERSION).tar.bz2 $(NAME)-$(VERSION)

orig: dist-bzip2
	mv sdist/$(NAME)-$(VERSION).tar.bz2 ../$(NAME)_$(CHANGELOG_VERSION).orig.tar.bz2

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(NAME)-$(VERSION))
