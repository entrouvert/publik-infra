#! /usr/bin/python3

import datetime
import itertools
import json
import os
import pickle
import re
import sys

import psycopg2
from prometheus_client import CollectorRegistry, Gauge
from prometheus_client.exposition import generate_latest

base_dir = '/var/lib/wcs'

registry = CollectorRegistry()
publik_count = Gauge('eo_publik_count', 'publik_count', ['site'], registry=registry)
publik_recent_count = Gauge(
    'eo_publik_recent_count', 'publik_recent_count', ['period', 'site'], registry=registry
)
publik_deprecations = Gauge(
    'eo_publik_deprecations', 'publik_deprecations', ['type', 'site'], registry=registry
)
publik_loggederrors = Gauge('eo_publik_loggederrors', 'publik_loggederrors', ['site'], registry=registry)
publik_loggederrors_uniq = Gauge(
    'eo_publik_loggederrors_uniq', 'publik_loggederrors_uniq', ['site'], registry=registry
)
publik_afterjobs = Gauge('eo_publik_afterjobs', 'publik_afterjobs', ['site'], registry=registry)

publik_long_processing = Gauge(
    'eo_publik_long_processing', 'publik_long_processing', ['period', 'site'], registry=registry
)

publik_cron_cpu_usage = Gauge(
    'eo_publik_cron_cpu_usage', 'publik_cron_cpu_usage', ['site'], registry=registry
)
publik_cron_memory_usage = Gauge(
    'eo_publik_cron_memory_usage', 'publik_cron_memory_usage', ['site'], registry=registry
)
publik_cron_sql_queries = Gauge(
    'eo_publik_cron_sql_queries', 'publik_cron_sql_queries', ['site'], registry=registry
)
publik_wide_tables = Gauge(
    'eo_publik_wide_tables', 'publik_wide_tables', ['site', 'tablename'], registry=registry
)

now = datetime.datetime.now(datetime.UTC)
time_5m = now - datetime.timedelta(minutes=5)
time_20m = now - datetime.timedelta(minutes=20)
time_1h = now - datetime.timedelta(hours=1)
time_1d = now - datetime.timedelta(days=1)


for tenant in itertools.chain(os.listdir(base_dir), os.listdir(os.path.join(base_dir, 'tenants'))):
    if (
        tenant.startswith('.')
        or tenant.startswith('_')
        or tenant.endswith('.invalid')
        or tenant
        in (
            'collectstatic',
            'cron-logs',
            'scripts',
            'skeletons',
            'spooler',
            'tenants',
        )
    ):
        continue
    dirname = os.path.join(base_dir, tenant)
    if not os.path.exists(dirname):
        dirname = os.path.join(base_dir, 'tenants', tenant)
    if not os.path.isdir(dirname):
        continue
    cfg = pickle.load(open(os.path.join(dirname, 'config.pck'), 'rb'), encoding='utf-8')
    if 'postgresql' not in cfg:
        continue
    psql_cfg = {}
    for k, v in cfg['postgresql'].items():
        if v and isinstance(v, (int, str)):
            psql_cfg[k] = v
    pgconn = psycopg2.connect(**psql_cfg)
    cur = pgconn.cursor()
    try:
        cur.execute('''SELECT COUNT(*) from wcs_all_forms WHERE status != 'draft' ''')
    except psycopg2.ProgrammingError:
        pass
    else:
        count = cur.fetchone()[0]
        publik_count.labels(tenant).set(count)

        for period, time in zip(('5m', '1h', '1d'), (time_5m, time_1h, time_1d)):
            cur.execute(
                '''SELECT COUNT(*) from wcs_all_forms
                            WHERE status != 'draft'
                              AND receipt_time > %(time)s
                              AND receipt_time < %(now)s''',
                {'time': time, 'now': now},
            )
            publik_recent_count.labels(period, tenant).set(cur.fetchone()[0])

    try:
        cur.execute('''SELECT SUM(occurences_count) FROM loggederrors WHERE deleted_timestamp IS NULL''')
    except psycopg2.ProgrammingError:
        pass
    else:
        count = cur.fetchone()[0]
        publik_loggederrors.labels(tenant).set(count or 0)

    try:
        cur.execute('''SELECT COUNT(*) FROM loggederrors WHERE deleted_timestamp IS NULL''')
    except psycopg2.ProgrammingError:
        pass
    else:
        count = cur.fetchone()[0]
        publik_loggederrors_uniq.labels(tenant).set(count)

    try:
        cur.execute(
            '''SELECT attrelid::regclass, COUNT(*)
                         FROM pg_attribute GROUP BY 1 HAVING COUNT(*) > 800
                        ORDER BY 2 DESC'''
        )
    except psycopg2.ProgrammingError:
        pass
    else:
        for table_name, count in cur.fetchall():
            publik_wide_tables.labels(tenant, table_name).set(count)

    try:
        table_names = []
        for objdef in ('formdefs', 'carddefs'):
            cur.execute(f'SELECT params FROM {objdef}')
            for row in cur.fetchall():
                params = pickle.loads(row[0])
                table_names.append(params.table_name)
        for period, time in zip(('5m', '20m'), (time_5m, time_20m)):
            long_processing_count = 0
            for table_name in table_names:
                # stalled formdata are unthawed after 1 hour, get those that have been processing
                # for more than 5 minutes
                cur.execute(
                    f'SELECT COUNT(*) FROM {table_name} WHERE workflow_processing_timestamp < %(timestamp)s',
                    {'timestamp': time},
                )
                long_processing_count += cur.fetchone()[0]
            publik_long_processing.labels(period, tenant).set(long_processing_count)
    except psycopg2.ProgrammingError:
        pass
    pgconn.close()

    afterjobs_dir = os.path.join(dirname, 'afterjobs')
    publik_afterjobs.labels(tenant).set(
        len(os.listdir(afterjobs_dir)) if os.path.exists(afterjobs_dir) else 0
    )

    dep_types = [
        'ezt',
        'jsonp',
        'python-condition',
        'python-expression',
        'python-prefill',
        'python-data-source',
        'script',
        'rtf',
        'actions',
        'fields',
        'csv-connector',
        'json-data-store',
    ]
    dep_filepath = os.path.join(dirname, 'deprecations.json')
    if os.path.exists(dep_filepath):
        with open(dep_filepath) as fp:
            deps = json.load(fp)
        for dep_type in dep_types:
            publik_deprecations.labels(dep_type, tenant).set(
                len([x for x in deps['report_lines'] if x['category'] == dep_type])
            )

    log_filenames = [
        os.path.join(dirname, f'cron-logs/{time_20m.year}/cron.log-{time_20m.strftime("%Y%m%d")}'),
        os.path.join(dirname, f'cron-logs/{now.year}/cron.log-{now.strftime("%Y%m%d")}'),
    ]
    if log_filenames[0] == log_filenames[1]:
        log_filenames = [log_filenames[0]]
    time_20m_prefix = time_20m.strftime('%Y-%m-%dT%H:%M:%S')
    now_prefix = now.strftime('%Y-%m-%dT%H:%M:%S')
    for log_filename in log_filenames:
        try:
            with open(log_filename) as fd:
                for line in fd:
                    if line[:19] < time_20m_prefix:
                        continue
                    if line[:19] >= now_prefix:
                        break
                    match = re.search(r'CPU time: ([\d\.]+)s / Memory: ([\d\.]+)M / SQL queries: (\d+)', line)
                    if not match:
                        continue
                    cpu, memory, sql = match.groups()
                    publik_cron_cpu_usage.labels(tenant).set(float(cpu))
                    publik_cron_memory_usage.labels(tenant).set(float(memory))
                    publik_cron_sql_queries.labels(tenant).set(int(sql))
                    break
        except FileNotFoundError:
            pass


print(generate_latest(registry).decode())
